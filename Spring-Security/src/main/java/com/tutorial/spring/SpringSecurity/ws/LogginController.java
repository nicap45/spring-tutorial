package com.tutorial.spring.SpringSecurity.ws;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LogginController {

    @GetMapping("login")
    public String login(){
        return  "login";
    }
}
